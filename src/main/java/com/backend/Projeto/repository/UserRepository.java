package com.backend.Projeto.repository;

import com.backend.Projeto.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * @author Vinny Parker
 */
public interface UserRepository extends JpaRepository<User, Long> {
}

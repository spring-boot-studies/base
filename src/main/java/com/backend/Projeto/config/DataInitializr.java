package com.backend.Projeto.config;

import com.backend.Projeto.entity.User;
import com.backend.Projeto.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * @author Vinny Parker
 */
@Component
public class DataInitializr implements ApplicationListener<ContextRefreshedEvent> {

    @Autowired
    UserRepository userRepository;

    @Override
    public void onApplicationEvent(ContextRefreshedEvent contextRefreshedEvent) {
        List<User> users = userRepository.findAll();

        if (users.isEmpty()) {
            this.createUsers("admin", "vinny.parker@gmail.com", "123456");
            this.createUsers("admin 2", "vinny.parker@gmail.com", "123456");
            this.createUsers("admin 3", "vinny.parker@gmail.com", "123456");
            this.createUsers("admin 4", "vinny.parker@gmail.com", "123456");
            this.createUsers("admin 5", "vinny.parker@gmail.com", "123456");




        }

    }

    public void createUsers(String name, String email, String password) {
        User user = new User(name, email, password);

        userRepository.save(user);

    }
}
